(function() {

    function ShoppingCartListItem(ShoppingCartRepository) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                cartItem: '='
            },
            templateUrl: '/js/components/shopping-cart-list-item/shopping-cart-list-item.html',
            link: function postLink($scope) {
                function remove() {
                    ShoppingCartRepository.removeCartItem($scope.cartItem);
                }

                function quantityChanged() {
                    console.log('changed', arguments, $scope.cartItem.quantity)
                    ShoppingCartRepository.updateCartItem($scope.cartItem);
                }

                $scope.remove = remove;
                $scope.quantityChanged = quantityChanged;
            }
        };
    }

    angular.module('app')
        .directive('shoppingCartListItem', [
            'ShoppingCartRepository',
            ShoppingCartListItem
        ]);

})();
