(function() {

    function ProductListItem(ShoppingCartRepository) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                product: '='
            },
            templateUrl: '/js/components/product-list-item/product-list-item.html',
            link: function postLink($scope) {
                function reset() {
                    $scope.quantity = 0;
                }

                function addToCart() {
                    ShoppingCartRepository.addItem(
                        $scope.product,
                        $scope.quantity
                    );

                    reset();
                }

                function noItemsSelected() {
                    return $scope.quantity <= 0;
                }

                $scope.addToCart = addToCart;
                $scope.noItemsSelected = noItemsSelected;

                reset();
            }
        };
    }

    angular.module('app')
        .directive('productListItem', [
            'ShoppingCartRepository',
            ProductListItem
        ]);

})();
