(function() {

    function ShoppingCart($state, ShoppingCartRepository) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '/js/components/shopping-cart/shopping-cart.html',
            link: function postLink($scope) {
                function navigateToShoppingCart() {
                    $state.go('shoppingCart');
                }

                function setItemCount() {
                    var items = ShoppingCartRepository.getItems();

                    $scope.itemCount = items.length;
                }

                $scope.$on('shopping-cart/changed', setItemCount);

                $scope.navigateToShoppingCart = navigateToShoppingCart;

                setItemCount();
            }
        };
    }

    angular.module('app')
        .directive('shoppingCart', [
            '$state',
            'ShoppingCartRepository',
            ShoppingCart
        ]);

})();
