(function () {

    function LocalStorageService($cookies) {
        function getObject(key) {
            return $cookies.getObject(key);
        }

        function putObject(key, object) {
            return $cookies.putObject(key, object);
        }

        return {
            getObject: getObject,
            putObject: putObject
        };
    }

    angular.module('app')
        .service('LocalStorageService', [
            '$cookies',
            LocalStorageService
        ]);

})();
