(function() {

    function defineRoutes($stateProvider, $urlRouterProvider) {
        $urlRouterProvider
            .when('', '/products')
            .when('/', '/products');

        $stateProvider
            .state('root', {
                name: 'root',
                url: '/',
                views: {
                    'appView@': {
                        templateUrl: '/js/views/appview/appview.html'
                    }
                }
            })
            .state('productList', {
                name: 'productList',
                parent: 'root',
                url: 'products',
                views: {
                    'mainView@root': {
                        templateUrl: '/js/views/product-list/product-list.html'
                    }
                }
            })
            .state('shoppingCart', {
                name: 'shoppingCart',
                parent: 'root',
                url: 'shopping-cart',
                views: {
                    'mainView@root': {
                        templateUrl: '/js/views/shopping-cart/shopping-cart.html'
                    }
                }
            })
            .state('orderComplete', {
                name: 'orderComplete',
                parent: 'root',
                url: 'thank-you-for-your-order',
                views: {
                    'mainView@root': {
                        templateUrl: '/js/views/shopping-cart/order-complete.html'
                    }
                }
            });
    }

    angular.module('app')
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            defineRoutes
        ]);
})();
