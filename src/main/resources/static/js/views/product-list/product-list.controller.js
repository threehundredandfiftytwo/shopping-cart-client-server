(function () {
    function ProductListController(SaleableItemRepository) {
        var vm = this;

        function itemsLoaded(productList) {
            vm.productList = productList;
        }

        SaleableItemRepository
            .getItems()
            .then(itemsLoaded);
    }

    angular.module('app')
        .controller('ProductListController', [
            'SaleableItemRepository',
            ProductListController
        ]);
}());
