(function () {
    function ShoppingCartController($scope, $state, OrderRepository, ShoppingCartRepository) {
        var vm = this;

        function hasCart() {
            return Boolean(vm.cartItems);
        }

        function hasItems() {
            return vm.cartItems.length > 0;
        }

        function isCartEmpty() {
            return !hasCart() || !hasItems();
        }

        function setItems() {
            vm.cartItems = ShoppingCartRepository.getItems();
        }

        function navigateToOrderCompletePage() {
            $state.go('orderComplete');
        }

        function confirmOrder() {
            OrderRepository
                .submitOrder(vm.cartItems)
                .then(ShoppingCartRepository.clear)
                .then(navigateToOrderCompletePage);
        }

        $scope.$on('shopping-cart/changed', setItems);

        setItems();

        vm.isCartEmpty = isCartEmpty;
        vm.confirmOrder = confirmOrder;
    }

    angular.module('app')
        .controller('ShoppingCartController', [
            '$scope',
            '$state',
            'OrderRepository',
            'ShoppingCartRepository',
            ShoppingCartController
        ]);
}());
