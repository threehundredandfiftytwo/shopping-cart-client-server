(function () {

    function SaleableItemRepository($http) {
        
        // this might be handled by an interceptor
        function parseData(response) {
            return response.data;
        }

        // this might be handled by an interceptor
        function parseItemList(responseData) {
            return responseData._embedded['saleable-items'];
        }

        function getItems() {
            return $http({
                method: 'GET',
                url: 'http://localhost:8081/saleable-items'
            })
            .then(parseData)
            .then(parseItemList);
        }

        return {
            getItems: getItems
        };
    }

    angular.module('app')
        .service('SaleableItemRepository', [
            '$http',
            SaleableItemRepository
        ]);

})();
