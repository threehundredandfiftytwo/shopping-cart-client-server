(function () {

    function OrderRepository($q) {
        
        function submitOrder() {
            return $q.when();
        }

        return {
            submitOrder: submitOrder
        };
    }

    angular.module('app')
        .service('OrderRepository', [
            '$q',
            OrderRepository
        ]);

})();
