(function () {

    function ShoppingCartRepository($rootScope, LocalStorageService) {
        var CART_KEY = 'shopping-cart-items';
        var cart;

        function loadCart() {
            var existingCart = LocalStorageService.getObject(CART_KEY);

            cart = existingCart || {};
        }

        function saveCart() {
            LocalStorageService.putObject(CART_KEY, cart);

            $rootScope.$broadcast('shopping-cart/changed');
        }

        function isItemInCart(itemId) {
            return typeof cart[itemId] !== 'undefined';
        }

        function cartItem(item, quantity) {
            return {
                item: item,
                quantity: quantity
            };
        }

        function storeItem(itemId, item) {
            cart[itemId] = item;
        }

        function computeNewQuantity(itemId, quantity) {
            return isItemInCart(itemId) ?
                cart[itemId].quantity + quantity :
                quantity;
        }

        function addItem(item, quantity) {
            var newQuantity = computeNewQuantity(item.uuid, quantity);

            storeItem(item.uuid, cartItem(item, newQuantity));

            saveCart();
        }

        function removeCartItem(cartItem) {
            delete cart[cartItem.item.uuid];

            saveCart();
        }

        function cartItemsToArray() {
            return Object.keys(cart).map(function (itemId) {
                return cart[itemId];
            });
        }

        function getItems() {
            return cartItemsToArray();
        }

        function clear() {
            cart = {};

            saveCart();
        }

        function updateCartItem(cartItem) {
            cart[cartItem.item.uuid] = cartItem;

            saveCart();
        }

        loadCart();

        return {
            addItem: addItem,
            clear: clear,
            getItems: getItems,
            removeCartItem: removeCartItem,
            updateCartItem: updateCartItem
        };
    }

    angular.module('app')
        .service('ShoppingCartRepository', [
            '$rootScope',
            'LocalStorageService',
            ShoppingCartRepository
        ]);

})();
