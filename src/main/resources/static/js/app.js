(function() {
    angular.module('app', [
        'ngCookies',
        'ui.router'
    ]);
  
    angular.element(document).ready(function () {
        angular.bootstrap(document, ['app']);
    });
})();
