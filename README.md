# README #

This is an Angular application that displays a list of products, allows the user to add items to a shopping cart, and to complete an order.

### Environment dependencies ###
- Gradle 3.5 [installation instructions](https://gradle.org/install)
- JDK 1.8 [installation instructions](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
- The shopping-cart-api-server: [repo and getting started instructions](https://bitbucket.org/threehundredandfiftytwo/shopping-cart-api-server)

### Get started ###
1. Make sure the shopping-cart-api-server is running (instructions in the [repo README](https://bitbucket.org/threehundredandfiftytwo/shopping-cart-api-server)).

2. Start the static content application server:
 
    ```
    ./gradle bootRun
    ```

3. The application will be available at [http://localhost:8080](http://localhost:8080)